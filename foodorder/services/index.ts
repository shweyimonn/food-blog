import axios from 'axios'
import { useRouter } from 'next/router'



export const getFoods = async () => {
    const foods: any= await axios.get('http://localhost:8080/foods')
    return foods.data;
}
export const getFoodsByStyle = async (inputPara : any) => {
    const foods: any= await axios.get('http://localhost:8080/foods',{
        params: { 
             style: inputPara
        }
    })
    return foods.data;
}


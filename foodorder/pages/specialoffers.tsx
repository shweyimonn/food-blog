import { FC, useEffect, useState } from "react"
import { getFoods } from "../services"
import { useRouter } from 'next/router'
import Navbar from '../components/navbar'
import Footer from '../components/footer'
const enum saleType {
    Normal = 0,
    Discount = 1,
    OutOfStock = 2  }

const FoodList: FC = () => {
    const [foods, setFoods] = useState<any[]>([])
    useEffect(() => {
        const exec = async () => {
            const foodData: any[] = await getFoods();
            setFoods(foodData)
        }
        exec()
    }, [foods])

    const router = useRouter();
const data = router.query;
    console.log(data);
    return (
        <>

<Navbar>
      </Navbar>
      <main>
        <div className="row m-5 p-5">
          <h1>Special Offer Menu </h1>

        </div>
                <div className="row m-5 p-5">
                    {foods.filter(x=>x.type === saleType.Discount).map(food => {
                        return (
                            
                            <div className="card m-3 p-3 col-md-4" style={
                                { width: '18rem' }
                            }>
                                <div className="card-head">
                                    <h4 className="card-text align-content-center">{food.name}</h4>

                                </div>
                                <div className="card-body">
                                    <img className="card-img-top" src={food.photoUrl} alt="Card image cap" />

                                </div>
                                    <h5 className="card-title">{food.price}$</h5>
                                
                                    <button className=" btn btn-info text-light" >Order Now</button>
                            </div>
                        )
                    })}
                </div>
            </main>
            <Footer></Footer>

        </>
    )
}

export default FoodList;    
import { FC, useEffect, useState } from "react"
import { getFoods, getFoodsByStyle } from "../services"
import Popup from 'reactjs-popup';

import { useRouter } from 'next/router'
import Navbar from '../components/navbar'
import Footer from '../components/footer'
const enum saleType {
    Normal = 0,
    Discount = 1,
    OutOfStock = 2
}


const FoodList: FC = () => {
    const router = useRouter();

    var FoodStyle = router.query.style;
    const [foods, setFoods] = useState<any[]>([])
    const [charge, setCharge] = useState<any>({
        changeAmount: 0,
        totalChargeAmount: 0
    });
    useEffect(() => {
        const exec = async () => {
            const foodData: any[] = await getFoodsByStyle(FoodStyle);
            setFoods(foodData)
        }
        exec()
    }, [foods])


    const handleChange = (e: any, food: any) => {
        setCharge({
            changeAmount: e.target.value,
            totalChargeAmount: e.target.value * food.price
        })
    }

    const submitOrder = () => {
        alert(charge.totalChargeAmount)
    }


    return (
        <>
            <Navbar>
            </Navbar>
            <main>
                <div className="row m-5 p-5">
                    <h1>{FoodStyle} - Food List</h1>

                </div>

                <div className="row m-5 p-5">
                    {foods.map(food => {
                        return (

                            <div className="card m-3 p-3 col-md-4" style={
                                { width: '18rem' }
                            }>
                                <div className="card-head">
                                    <h4 className="card-text align-content-center">{food.name}</h4>

                                </div>
                                <div className="card-body">
                                    <img className="card-img-top" src={food.photoUrl} alt="Card image cap" />

                                </div>
                                <h5 className="card-title">{food.price}$</h5>

                                <Popup trigger={<button className=" btn btn-info text-light" >Order Now</button>} position="right center" >
                                    <div className="row col-md-6 bg-info text-white px-2 py-3">
                                        <div className="col-md-6">
                                            Item-Name
                                        </div>
                                        <div className="col-md-6">
                                            {food.name}
                                        </div>
                                        <div className="col-md-6">
                                            Item-Price
                                        </div>
                                        <div className="col-md-6">
                                            {food.price}$
                                        </div>
                                        <div className="col-md-12">
                                            <input type={'number'} className="form-field" onChange={(e) => handleChange(e, food)} />
                                        </div>
                                        <div className="col-md-6">
                                            Charge Amount
                                        </div>
                                        <div className="col-md-6">
                                            {charge.totalChargeAmount}$
                                        </div>
                                        <div className="col-md-12">
                                            <button className="btn-secondary" onClick={submitOrder}>Order</button>
                                        </div>
                                    </div>
                                </Popup>
                            </div>
                        )
                    })}
                </div>
            </main>
        </>
    )
}

export default FoodList;    
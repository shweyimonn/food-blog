import { FC, useEffect, useState } from "react"
import { getFoods } from "../services"
import { useRouter } from 'next/router'
import Navbar from '../components/navbar'
import Footer from '../components/footer'
import Link from "next/link";

const enum saleType {
  Normal = 0,
  Discount = 1,
  OutOfStock = 2
}

const Home: FC = () => {
  const [foods, setFoods] = useState<any[]>([])
  useEffect(() => {
    const exec = async () => {
      const foodData: any[] = await getFoods();
      setFoods(foodData.filter(
        (f, i, arr) => arr.findIndex(t => t.style === f.style) === i))
    }
    exec()
  }, [foods])


  return (
    <>
      <Navbar>
      </Navbar>
      <main>
        <div className="row m-5 p-5">
          <h1>Menu Categories</h1>

        </div>
        <div className="row m-5 p-5">
          {foods.filter(x => x.type === saleType.Normal).map(food => {
            return (

              <div className="card m-3 p-3 col-md-4" style={
                { width: '18rem' }
              }>
                <div className="card-head">
                  <h4 className="card-text align-content-center">{food.style} MENU</h4>
                
                </div>
                <div className="card-body">
                  <img className="card-img-top" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRwIHClA6pzp8ypb2yi2oOKLm_posvIUpQitg&usqp=CAU" alt="Card image cap" />
                </div>
                  <Link href={{ pathname: '/listbystyle', query: { style: food.style } }}><a  className=" btn btn-info text-light" >View Detail</a></Link>

              </div>
            )
          })}
        </div>
      </main>
      <Footer></Footer>
    </>
  )
}

export default Home;    
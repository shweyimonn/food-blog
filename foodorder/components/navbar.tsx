export default function Layout() {
    return (
        <>
                <nav className="navbar navbar-expand-lg navbar-light bg-light ">
                <img className="mx-4" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTII8a7X4Jt-Wq_GgS8V8nMKmLqp-0N0-D-RfJJitTGtxt_GpFCtbLhSjrniEH0oih-uO8&usqp=CAU" alt="Card image cap" style={{ width: '4rem' }}/>
                    <a className="navbar-brand" href="/">Food Center</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav">
                            <li className="nav-item mx-4">
                                <a className="nav-link active" href="/">Home </a>
                            </li>
                            <li className="nav-item ">
                                <a className="nav-link mx-4" href="/foodlist">Food</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link mx-4" href="/specialoffers">Special Offers</a>
                            </li>
                         
                        </ul>
                    </div>
                </nav>
        </>
    )
}